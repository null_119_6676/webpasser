/*
 * 系统名称: 
 * 模块名称: webpasser.project
 * 类 名 称: CustomDemoRule.java
 *   
 */
package com.hxt.webpasser.regular;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hxt.webpasser.transport.xml.Rule;

/**
 * 功能说明: 自定义rule处理链例子 <br>
 * 系统版本: v1.0 <br>
 * 作者: hanxuetong <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class Custom360ZhushouDemoRule implements DecideRule{

	public List<Object> handle(Rule rule, List<Object> contentList, Map valueMap) {
		// valueMap 默认会有 fetchUrl 和  taskName的值
		String fetchUrl=String.valueOf(valueMap.get("fetchUrl"));
		String taskName=String.valueOf(valueMap.get("taskName"));
		String value=rule.getValue();
	/*	if(contentList!=null)
		{

			for(int i=0;i<contentList.size();i++)
			{
				String newCon=String.valueOf(contentList.get(i));
				
				contentList.set(i, newCon);
			}
				
		}*/
		if(fetchUrl.indexOf("?")==-1){
			 List<Object> list=new ArrayList<Object>();
			for(int i=1;i<=50;i++){
				String url=fetchUrl+"?page="+i;
				list.add(url);
			}
			return list;
		}

		return contentList;
	}
	
/*	private static String getUrlPage(int jumpPage,String localUrl){
		
		String preUrl=StringUtil.cutNotContainStartAndEnd(localUrl, "", "?page");
		
		return preUrl+"?page="+jumpPage;
	}
	
	public static void main(String[] args) {
		
		String p=getUrlPage(1, "http://zhushou.360.cn/list/index/cid/1/");
		System.out.println(p);
	}*/
	
}
